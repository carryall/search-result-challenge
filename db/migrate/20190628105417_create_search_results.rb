class CreateSearchResults < ActiveRecord::Migration[5.2]
  def change
    create_table :search_results do |t|
      t.string :keyword, index: true
      t.integer :report_id, index: true
      t.integer :no_of_adwords
      t.integer :no_of_links
      t.integer :no_of_results
      t.string :result_stats
      t.text :page_cache
      
      t.timestamps
    end
    add_foreign_key :search_results, :reports
  end
end
