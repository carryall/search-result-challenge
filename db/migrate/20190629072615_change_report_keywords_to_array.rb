class ChangeReportKeywordsToArray < ActiveRecord::Migration[5.2]
  def change
    change_column :reports, :keywords, "varchar[] USING (string_to_array(keywords, ','))"
  end
end
