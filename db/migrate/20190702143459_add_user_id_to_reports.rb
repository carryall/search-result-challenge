class AddUserIdToReports < ActiveRecord::Migration[5.2]
  def change
    add_column :reports, :user_id, :integer
    add_foreign_key :reports, :users
  end
end
