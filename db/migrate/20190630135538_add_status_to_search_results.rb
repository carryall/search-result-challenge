class AddStatusToSearchResults < ActiveRecord::Migration[5.2]
  def change
    add_column :search_results, :status, :integer, default: 0
  end
end
