require "rails_helper"

RSpec.describe User, type: :model do
  let(:user) { Fabricate.create(:user) }

  describe "#create_report!" do
    let(:keywords) { FFaker::BaconIpsum.words }

    it "create user report with specified keywords" do
      report = user.create_report!(keywords)
      expect(user.reports).to include(report)
      expect(report.keywords).to eq(keywords)
    end
  end
end
