require "rails_helper"

RSpec.describe Report, type: :model do
  let(:report) { Fabricate.build(:report) }

  describe "#create" do
    it "generate search result after create" do
      expect(report).to receive(:generate_search_results)
      report.run_callbacks(:create)
    end
  end
end
