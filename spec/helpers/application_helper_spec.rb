require "rails_helper"

RSpec.describe ApplicationHelper, type: :helper do
  describe "#active_menu_class" do
    before { allow(helper.request).to receive(:path).and_return "/some-path" }
    it "returns active if provided path is current path" do
      expect(helper.active_menu_class('/some-path')).to eq("active")
    end

    it "returns nil if provided path is not current path" do
      expect(helper.active_menu_class('/not-current-path')).to be_nil
    end
  end
end
