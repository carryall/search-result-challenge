Fabricator(:search_result) do
  id            1
  keyword       "MyString"
  report_id     1
  no_of_adwords 1
  no_of_links   1
  no_of_results 1
  result_stats  "MyString"
  page_cache    "MyText"
  created_at    "2019-06-29 22:51:08"
  updated_at    "2019-06-29 22:51:08"
end
