Rails.application.routes.draw do
  devise_for :users
  get '/', to: 'reports#new', as: 'root'
  get 'reports/:report_id/search_results/:id/page_cache', to: 'search_results#page_cache', as: 'page_cache'
  resources :reports, only: [:index, :create, :show]
end
