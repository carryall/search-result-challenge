class SearchResultsController < ApplicationController
  before_action :authenticate_user!

  def page_cache
    report = current_user.reports.find(params[:report_id])
    @search_result = report.search_results.find(params[:id])
  end
end
