require "csv"

class ReportsController < ApplicationController
  before_action :authenticate_user!

  def index
    @reports = current_user.reports
  end

  def new
    @report = current_user.reports.new()
  end

  def create
    if file = params[:file]
      keywords = keywords_from_csv(file.path)
      report = current_user.create_report!(keywords)
      redirect_to(report)
    else
      render file: "public/422.html"
    end
  end

  def show
    @report = current_user.reports.find(params[:id])
  end

  private
  def keywords_from_csv(file_path)
    csv = CSV.parse(csv_text(file_path), :headers => false)
    keywords = csv.map { |keyword| keyword[0] }
  end

  def csv_text(file_path)
    File.read(file_path)
  end

end
