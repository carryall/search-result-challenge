class Report < ApplicationRecord
  has_many :search_results
  belongs_to :user

  validates :keywords, presence: true

  after_create :generate_search_results

  def generate_search_results
    self.keywords.each do |keyword|
      SearchResult.create!(keyword: keyword, report: self)
    end
  end
end
