class SearchResult < ApplicationRecord
    enum status: [:pending, :ready]

    belongs_to :report
    
    validates :keyword, :report_id, presence: true
    
    after_create_commit :scrape_result

    def scrape_result
        ResultScrapeJob.perform_later(self.id, self.keyword)
    end
end
