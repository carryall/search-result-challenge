module ApplicationHelper
  def active_menu_class(path)
    'active' if current_page?(path)
  end
end
