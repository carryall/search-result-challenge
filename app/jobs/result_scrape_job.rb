class ResultScrapeJob < ActiveJob::Base

  rescue_from(OpenURI::HTTPError) do
    retry_job wait: 5.minutes, queue: :default
  end
  
  def perform(result_id, keyword)
    require "openssl"
    doc = Nokogiri::HTML(
      open("https://www.google.com/search?q=" + keyword + "&hl=en-TH",
           :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE,
           "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36")
    )
    result_stats = doc.css("#resultStats")
    links = doc.css("a")
    results = doc.css(".g")
    adWords = doc.css(".ads-ad")

    result = SearchResult.find(result_id)
    if (result)
      result.page_cache = doc
      result.no_of_adwords = adWords.size
      result.no_of_links = links.size
      result.no_of_results = results.size
      result.result_stats = result_stats.text
      result.status = 'ready'
      result.save
    end
  end
end
